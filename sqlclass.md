There are important function name that come out with this SQL Class

getResultRowArray(),
getResultOneRowArray(),
getRowCount(),
getUpdate(),
InsertData(),
getDelete().

There 6 functions have to remain the same name as it is.
because I have use the Class SQL intensively in the implementation 
of php7-phhsystem for future projects.

if the name is different, it will causing inconsistency for all php7-phhsystem
implemetation for your OOP Class. you can not extend you Class put your Class in the
project.

Class SQL extends Dbh {

    protected $sql; // class common variable

    public function __construct($sql) {//from program line inject into constructor
        $this->sql = $sql; //assign the protected $sql ($this->sql to the value of
        // injected value $sql.
    }

    public function getResultRowArray() {
        $resultset = array(); //define empty array
        $sql = $this->sql; // assign private $sql by the value of protected $sql
        //                  ($this->sql)
        //echo "\$sql = $sql <br>";
        $stmt = $this->connect()->prepare($sql); // $stmt must be local variable
        $stmt->execute();
        if ($stmt->rowCount()) {
            $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $resultset = $row;

        } else {
            // do nothing;
            //echo "no result on SQL <br>";
        }

        return $resultset;
    }

    public function getResultOneRowArray() {
        $row = array();
        $sql = $this->sql;

        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount()) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
        }

        return $row;
    }

    public function getRowCount() {

        $sql = $this->sql;

        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $number_of_rows = $stmt->fetchColumn();

        return $number_of_rows;
    }

    public function getUpdate() {

        $sql = $this->sql;

        $stmt = $this->connect()->prepare($sql);

        if ($stmt->execute()) {
            $result = 'updated';
        } else {
            $result = 'update fail';
        }
        return $result;
    }

    public function InsertData() {

        $sql = $this->sql;

        $stmt = $this->connect()->prepare($sql);

        if ($stmt->execute()) {
            $result = 'insert ok!';
        } else {
            $result = 'insert fail';
        }

        return $result;
    }
    public function getDelete() {

        $sql = $this->sql;

        $stmt = $this->connect()->prepare($sql);

        if ($stmt->execute()) {
            $result = 'deleted';
        } else {
            $result = 'delete failed';
        }
        return $result;
    }    