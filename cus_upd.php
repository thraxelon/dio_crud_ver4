<?php
include_once 'class/customers.inc.php';
$page_title = "Update Customer";

#checks if CID is carried over
$id = isset($_GET['cid']) ? $_GET['cid'] : die('ERROR: You can\'t reach this page this way. <br> 
												 Please do it the  <a href="cus_list.php">right way</a>.');
#includes header
include_once 'lib/pg_header.php';

#creates new customer object using POST data
$customer = new Customer($_POST);		

#begins update function
$updCustomer = $customer->update($id);  //--> check customers.inc.php

echo $updCustomer."<br>";
echo "Click <a href='cus_list.php'>here</a> to return.</div>";

#includes footer
include_once "lib/pg_footer.php";
?>
