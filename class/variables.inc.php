<?php

Class SQL extends DB{
	#initialize class properties
	protected $sql;

	#end of initialization

	public function __construct($sql){ 	// put your action that needed to be done when the class is called here
		$this->sql = $sql;				// insert data from input into protected variable
	}

	public function getResultRowArray(){
		$rowResult = array();			// initialize empty array
		$sql = $this->sql;				// get query string
		#start preparing query
		$stmt = $this->connect()->prepare($sql); //prepares query
		$stmt->execute();				//executes prepared query
		#end preparing query
		if ($stmt->rowCount()) {		//check if there's array returned
			$row = $stmt->fetchAll(PDO::FETCH_ASSOC);	//fetches all rows as arrays

		}
		return $row;					//returns array
	}

	public function getResultOneRowArray(){
		$rowResult = array();			// initialize empty array
		$sql = $this->sql;				// get query string
		#start preparing query
		$stmt = $this->connect()->prepare($sql); // prepares query
		$stmt->execute();				//executes prepared query
		#end preparing query
		if ($stmt->rowCount()){			// check if there's array returned
			$row = $stmt->fetch(PDO::FETCH_ASSOC);		//fetches single row as array
		}
		return $row;					//returns array
	}

	public function getRowCount(){
		$sql = $this->sql;			

		$stmt = $this->connect()->prepare($sql);
		$stmt->execute();				//executes prepared query
		$rowNumbers = $stmt->fetchColumn(); // get number of rows

		return $rowNumbers;
	}

	public function getUpdate(){
		$sql = $this->sql;

		$stmt = $this->connect()->prepare($sql);

		if ($stmt->execute()){			//return TRUE if able to execute
			$result = 'updated';
		}else{
			$result = 'update fail';
		}
		return $result;
	}

	public function InsertData(){
		$sql = $this->sql;

		$stmt = $this->connect()->prepare($sql);

		if ($stmt->execute()){			//return TRUE if able to execute
			$result = 'insert ok!';
		}else{
			$result = 'insert fail';
		}
		return $result;
	}

	public function getDelete(){
		$sql = $this->sql;

		$stmt = $this->connect()->prepare($sql);

		if ($stmt->execute()){			//return TRUE if able to execute
			$result = 'delete ok!';
		}else{
			$result = 'delete fail';
		}
		return $result;
	}
}

Class SQLBindParam extends SQL {

    protected $sql;
    protected $bindparamArray;

    public function __construct($sql, $bindparamArray) { //---> this function needs a query and $POST Values to activate

        parent::__construct($sql);
        $this->bindparamArray = $bindparamArray;
    }

    public function InsertData2() {

        $sql = $this->sql;								//--> fetches the Query into local function
        $bindparamArray = $this->bindparamArray;		//--> fetches the Array that would be bound to Parameters
        $stmt = $this->connect()->prepare($sql);		//--> prepares the Query
        
        $dtime = date('Y-m-d H:i:s');
        #echo $sql."<br><br>";
        unset($bindparamArray['submit']);
//        unset($bindparamArray['credit_used']);
//        unset($bindparamArray['one_do_one_inv']);
//        unset($bindparamArray['cannot_migrate']);
//        unset($bindparamArray['regular']);
//        unset($bindparamArray['nobusy']);
//        echo " print_r(\$bindparamArray) := <br>";
//        print_r($bindparamArray);
//        echo "<br>";
//        $para = "";

        foreach ($bindparamArray as $key => $value) {	//--> transfers values from Array into their own variables
            # code...
            ${$key} = $value;							//--> creates a variable based on $key value as thename
            $bindValue = $key;							//--> inserts $key value into the var. this is to call again during 													  bindParam
            # echo $key." = ".$value."<br>";
            # $bindParamdata = "bindParam(':$key', $value)<br>";
            # echo "\$bindParamdata = $bindParamdata <br>";
            
            # print_r($bindParamdata);

            #########################################################
            # this line not successful, how to check in the future
            //  $stmt->bindParam(":$key", $value);
            ######################################################
            ######################################################
            # this is working, if there's another one, can discuss
            $stmt->bindParam(":{$key}",$$bindValue);	//--> binds value of each parameter with the above value.
            ######################################################
//            $para .= ":$key, $value <br>";
        }
//        echo "===================================<br>";
//        echo "\$para is listed as below :-<br> $para <br>";

        #Begin bounding parameters#
        # $stmt->bindParam(":accno", $accno);
        # $stmt->bindParam(":co_name", $co_name);
        # $stmt->bindParam(":co_no", $co_no);
        # $stmt->bindParam(":co_code", $co_code);
        # $stmt->bindParam(":adr1", $adr1);
        # $stmt->bindParam(":adr2", $adr2);
        # $stmt->bindParam(":adr3", $adr3);
        # $stmt->bindParam(":country", $country);
        # $stmt->bindParam(":salesphone", $salesphone);
        # $stmt->bindParam(":salesfax", $salesfax);
        # $stmt->bindParam(":saleshandphone", $saleshp);
        # $stmt->bindParam(":salesemail", $salesmail);
        # $stmt->bindParam(":salesatn", $salesattn);
        # $stmt->bindParam(":accphone", $accphone);
        # $stmt->bindParam(":accfax", $accfax);
        # $stmt->bindParam(":acchandphone", $acchp);
        # $stmt->bindParam(":accemail", $accmail);
        # $stmt->bindParam(":accatn", $accattn);
        # $stmt->bindParam(":groups", $group);
        # $stmt->bindParam(":aid_cus", $salesperson);
        # $stmt->bindParam(":terms", $terms);
        # $stmt->bindParam(":currency", $currency);
        # $stmt->bindParam(":creditlimit", $credit_limit);
        # $stmt->bindParam(":company", $company);
        # $stmt->bindParam(":status", $status);
        # $stmt->bindParam(":dtime", $tdate);
        # $stmt->bindParam(":remarks", $remarks);
        #end bounding parameters#


//        echo "=====var_dump \$stmt==================<br>";
//        var_dump($stmt);
//        echo "=====end of var_dump \$stmt==================<br>";


        if ($stmt->execute()) {
            $result = 'insert ok!';
        } else {
            $result = 'insert fail';
        }
        return $result;
    }
}
?>