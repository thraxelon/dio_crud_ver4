<?php

class DB{
	#initialize properties
	private $host;
	private $username;
	private $password;
	private $db_name;
	private $charset;
	private $conn;
	#end initialization

	#begin connect function
	public function connect() {
		$this->host = "localhost";   			//server name
		$this->username = "phhsystem";			//user name to login into database
		$this->password = "cnAfahrenheit5t";	//password to login into database
		$this->db_name = "dio_crud";			//database name
		$this->charset = "utf8";				//charset used in this db
		$this->conn = null;						

		#begin connection
		try {
			$dsn = "mysql:host=".$this->host.";dbname=".$this->db_name.";charset=".$this->charset;
			$this->conn = new PDO($dsn, $this->username, $this->password);
			
			return $this->conn;
		} catch (PDOException $e) {				//if error show error message
			echo "Connection failed: ".$e->getMessage();
		}
		#end connection
	}
	#end connect function

}

