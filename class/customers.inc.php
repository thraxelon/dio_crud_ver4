<?php
include_once "dbh.inc.php";				//calls db connection function
include_once "variables.inc.php";		//calls query qctivity function

Class Customer{
	#begin initialize properties
	protected $getPostData;	//array that will contain $_POST datas.
	public $cid;			//customer id
	#end initialization


	function __construct($postdata=array()){ //--> Put tasks that will be done first here
		$this->getPostData = $postdata;  //initialize empty array
	}

	function create(){		//--> create a new customer data
		$postdata = $this->getPostData;	//insert $_POST data to properties
		#print_r($postdata);
		#echo "<br>";
		#get current datetime
		$dtime = date('Y-m-d H:i:s');

		#begin trimming data from array into variables.
		$accno = trim($postdata['accno']); 		#customer id
		$co_name = trim($postdata['co_name']); 	#customer/company name
		$co_no = trim($postdata['co_no']);		#customer Tax number
		$co_code = trim($postdata['co_code']);	#customer Initials
		$adr1 = trim($postdata['adr1']); 		#address1st  line
		$adr2 = trim($postdata['adr2']);		#address 2nd Line
		$adr3 = trim($postdata['adr3']);		#address 3rd line
		$country = trim($postdata['country']);	#company country
		$salesphone = trim($postdata['salesphone']);	#sales phone number
		$salesfax = trim($postdata['salesfax']);	#sales fax number
		$saleshp = trim($postdata['saleshp']);	#sales handphone number
		$salesmail = trim($postdata['salesmail']);	#sales email
		$salesatn = trim($postdata['salesattn']);	#sales name
		$accphone = trim($postdata['accphone']);	#accounting phone number
		$accfax = trim($postdata['accfax']);		#accounting fax number
		$acchp = trim($postdata['acchp']);		#accounting handphone number
		$accmail = trim($postdata['accmail']);	#accounting email
		$accatn = trim($postdata['accattn']);		#accounting name
		$groups = trim($postdata['group']);		#grouping code
		$aid_cus = trim($postdata['salesperson']);	#marketing code
		$terms = trim($postdata['terms']);		#payment terms
		$currency = trim($postdata['currency']);	#payment currency
		$creditlimit = trim($postdata['credit_limit']);#payment credit limits
		$company = trim($postdata['company']);	#company branch
		$status = trim($postdata['status']);		#customer status (active, hold, disabled)
		$remarks = trim($postdata['remarks']);	#remarksx
		#done trimming data

		#create query to insert data to db.
		$qr = "INSERT INTO customer_ptphh 
			   SET
			   accno ='{$accno}',
			   co_name ='{$co_name}', 
			    co_no='{$co_no}', 
			    co_code='{$co_code}', 
			    address1='{$adr1}', 
			    address2='{$adr2}', 
			    address3='{$adr3}', 
			    country='{$country}', 
			    telephone_sales='{$salesphone}', 
			    fax_sales='{$salesfax}', 
			    handphone_sales='{$saleshp}', 
			    email_sales='{$salesmail}', 
			    attn_sales='{$salesatn}', 
			    telephone_acc='{$accphone}', 
			    fax_acc='{$accfax}', 
			    handphone_acc='{$acchp}', 
			    email_acc='{$accmail}', 
			    attn_acc='{$accatn}', 
			    groups='{$groups}', 
			    aid_cus={$aid_cus}, 
			    terms='{$terms}', 
			    currency={$currency}, 
			    credit_limit={$creditlimit}, 
			    company='{$company}', 
			    status='{$status}',
			    date_created='{$dtime}',
			    remarks='{$remarks}'";
		#echo $qr;
		# do the insert activity
		$dbInsert = new SQL($qr);
		if ($dbInsert->InsertData()=="insert ok!"){
			$insResult = "<div class='alert alert-success'>Insert Successful.<br>";
		}else{
			$insResult = "<div class='alert alert-danger'>Failed to Insert Data!<br>";
		}
		return $insResult;		//TRUE or FALSE
	}

	function update($cid){		//--> Updates customer data, must contains CID value
		$postdata = $this->getPostData;	//insert $_POST data to properties
		$this->cid = $cid;


		#begin trimming data from array into variables.
		$accno = trim($postdata['accno']); 		#customer id
		$co_name = trim($postdata['co_name']); 	#customer/company name
		$co_no = trim($postdata['co_no']);		#customer Tax number
		$co_code = trim($postdata['co_code']);	#customer Initials
		$adr1 = trim($postdata['adr1']); 		#address1st  line
		$adr2 = trim($postdata['adr2']);		#address 2nd Line
		$adr3 = trim($postdata['adr3']);		#address 3rd line
		$country = trim($postdata['country']);	#company country
		$salesphone = trim($postdata['salesphone']);	#sales phone number
		$salesfax = trim($postdata['salesfax']);	#sales fax number
		$saleshp = trim($postdata['saleshp']);	#sales handphone number
		$salesmail = trim($postdata['salesmail']);	#sales email
		$salesatn = trim($postdata['salesattn']);	#sales name
		$accphone = trim($postdata['accphone']);	#accounting phone number
		$accfax = trim($postdata['accfax']);		#accounting fax number
		$acchp = trim($postdata['acchp']);		#accounting handphone number
		$accmail = trim($postdata['accmail']);	#accounting email
		$accatn = trim($postdata['accattn']);		#accounting name
		$groups = trim($postdata['group']);		#grouping code
		$aid_cus = trim($postdata['salesperson']);	#marketing code
		$terms = trim($postdata['terms']);		#payment terms
		$currency = trim($postdata['currency']);	#payment currency
		$creditlimit = trim($postdata['credit_limit']);#payment credit limits
		$company = trim($postdata['company']);	#company branch
		$status = trim($postdata['status']);		#customer status (active, hold, disabled)
		$remarks = trim($postdata['remarks']);	#remarksx
		#done trimming data

		#create query to insert data to db.
		$qr = "UPDATE customer_ptphh 
			   SET
			   accno ='{$accno}',
			   co_name ='{$co_name}', 
			    co_no='{$co_no}', 
			    co_code='{$co_code}', 
			    address1='{$adr1}', 
			    address2='{$adr2}', 
			    address3='{$adr3}', 
			    country='{$country}', 
			    telephone_sales='{$salesphone}', 
			    fax_sales='{$salesfax}', 
			    handphone_sales='{$saleshp}', 
			    email_sales='{$salesmail}', 
			    attn_sales='{$salesatn}', 
			    telephone_acc='{$accphone}', 
			    fax_acc='{$accfax}', 
			    handphone_acc='{$acchp}', 
			    email_acc='{$accmail}', 
			    attn_acc='{$accatn}', 
			    groups='{$groups}', 
			    aid_cus={$aid_cus}, 
			    terms='{$terms}', 
			    currency={$currency}, 
			    credit_limit={$creditlimit}, 
			    company='{$company}', 
			    status='{$status}',
			    remarks='{$remarks}'
			    WHERE cid={$this->cid}";

		# do the insert activity
		$dbUpdate = new SQL($qr);
		if ($dbUpdate->getUpdate()=="updated"){
			$updResult = "<div class='alert alert-success'>Update Successful.<br>";
		}else{
			$updResult = "<div class='alert alert-danger'>Failed to Update!<br>";
		}
		return $updResult;		//TRUE or FALSE	
	}

	function summonAll(){	//--> Fetches all rows from table Customer
		#settle query
		$qr = "SELECT * FROM customer_ptphh ORDER BY accno ASC";

		#execute query
		$dbSummon = new SQL($qr);
		$result = $dbSummon->getResultRowArray();
		return $result;
	}

	function summonLimit($offset, $limit){ //--> Fetches rows bound by offset and limit, used for Pagination, 
		#settle query
		$qr = "SELECT * FROM customer_ptphh ORDER BY accno ASC LIMIT {$offset},{$limit}";
		#echo $qr;
		#execute query
		$dbSummonLimit = new SQL($qr);
		$result = $dbSummonLimit->getResultRowArray();
		return $result;
	}

	function summonOne($cid){	//--> Fetches single row from table Customer
		#settle query
		$qr = "SELECT * FROM customer_ptphh WHERE cid={$cid} LIMIT 0,1";

		#execute query
		$dbsummonOne = new SQL($qr);
		$result = $dbsummonOne->getResultOneRowArray();
		return $result;

	}

	function countAll(){	//--> Counts the number of rows exists in Customer Table
		#settle query
		$qr = "SELECT count(*) FROM customer_ptphh";

		#execute query
		$dbCountRow = new SQL($qr);
		$result = $dbCountRow->getRowCount();
		return $result;

	}

	function byebye($cid){		//--> Deletes a row from Customer Table, must contain CID
		#settle query
		$qr = "DELETE FROM customer_ptphh WHERE cid ={$cid}";

		#execute query
		$dbBye = new SQL ($qr);
		if ($dbBye->getDelete()=="delete ok!") {
			$byeResult = "<div class='alert alert-success'>Delete Successful.<br>";
		}else{
			$byeResult = "<div class='alert alert-danger'>Failed to Delete Data!<br>";
		}
		return $byeResult;
	}

	function createParam(){     //--> Creates a new customer data, using bindParam Method.
		#settle query
		$postdata = $this->getPostData;
		$qr = "INSERT INTO customer_ptphh
			   SET
			   accno=:accno,   
			   co_name=:co_name,
			   co_no=:co_no,
			   co_code=:co_code,
			   address1=:adr1,
			   address2=:adr2,
			   address3=:adr3,
			   country=:country,
			   telephone_sales=:salesphone,
			   fax_sales=:salesfax,
			   handphone_sales=:saleshp,
			   email_sales=:salesmail,
			   attn_sales=:salesattn,
			   telephone_acc=:accphone,
			   fax_acc=:accfax,
			   handphone_acc=:acchp,
			   email_acc=:accmail,
			   attn_acc=:accattn,
			   groups=:group,
			   aid_cus=:salesperson,
			   terms=:terms,
			   currency=:currency,
			   credit_limit=:credit_limit,
			   company=:company,
			   date_created=:tdate,
			   status=:status,
			   remarks=:remarks";
		$dbInsert2 = new SQLBindParam($qr,$postdata); //--> calls a different class to bound above Parameters

		if($dbInsert2->InsertData2()=="insert ok!"){
			$result = "<div class='alert alert-success'>Insert Successful<br>";
		}else{
			$result = "<div class='alert alert-danger'>Failed to Insert Data<br>";

		}
		return $result;
	}
}




?>