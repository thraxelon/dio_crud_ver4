 <?php
include_once 'class/customers.inc.php';
$page_title = "List Customer";

//create db and objects
$customer = new Customer();

#includes header
include_once 'lib/pg_header.php';
#includes top paging
include "lib/pagination.php";

#this is to fetch table
$rRow = $customer->summonLimit($startLimit,$rowPage); //--> Check customers.inc.php

#this is for number of rows
$rCount = $customer->countAll();
#$rowResult = $stmt->fetchAll(PDO::FETCH_ASSOC);

	if ($rCount>0){ //--> do the following codes if returned rows are not zero
		echo "<table class='table table-hover' border='0' style='margin-left: auto; margin-right: auto;'>
				
				<tr></tr>
				<tr>
					<th style='padding: 5px; text-align: center' p>Account No</th>
					<th>Company Name</th>
					<th>Address</th>
					<th>P.I.C</th>
					
					<th style='text-align: center;border-right: none; border-left: none' width ='18%' align='center'>Action</th>
					
				</tr>";
		#extracting row from db
		#foreach ($rowResult as $record) {
		#	echo "<tr>";
		#	foreach ($record as $key => $value) {
		#		echo "<td>".$value."</td>";
		#	}
		#	echo "</tr>";
		#}
		foreach ($rRow as $value) { //--> begin loop to fill the table with data from db
		#while ($rowResult = $stmt->fetch(PDO::FETCH_ASSOC)) {
			#extract($rowResult);
			echo "<tr>
				  <td class='rowpad'>{$value['accno']}</td>
				  <td class='rowpad'>{$value['co_name']}</td>
				  <td class='rowpad'>{$value['address1']}</td>
				  <td class='rowpad'>{$value['attn_sales']}</td>
				  <td class='rowpad' style='text-align:center'>
				  			<a href='cus_details.php?cid={$value['cid']}&edt=no' class='btn btn-primary left-margin'>Details</a> 
				  			<a href='cus_details.php?cid={$value['cid']}&edt=yes' class='btn btn-info left-margin'> Edit </a> 
				  			<a href='cus_delete.php?cid={$value['cid']}' OnClick=\"return confirm('Are you sure? Data will be lost!')\" class='btn btn-danger delete-object'> Delete </a>
				  			</td>
				  
				  </tr>
				  <tr padding='9px'></tr>";


		}
		echo "</table>";
	}
	#includes bottom paging
	include "lib/pagination.php";
?>
<?php 
#includes footer
include_once "lib/pg_footer.php"; 
?> 
<br> 
</html>